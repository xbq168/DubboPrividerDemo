package com.dubbo.demo.provider;

import com.dubbo.demo.api.IHelloWorld;

/**
 * 实现接口
 * @author xbq
 * 可以调用 IHelloWorld ，是因为 引入了的 api项目的依赖
 */
public class HelloWorldServiceImpl implements IHelloWorld{

	@Override
	public String sayHello(String name) {
		return "Hello World!" + name;
	}
	
}
