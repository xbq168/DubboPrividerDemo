package com.dubbo.demo.provider;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;
/**
 * 启动生产者服务
 * @author xbq
 */
public class Main {

	public static void main(String[] args) {
		// 引用xml文件
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("provider.xml");
		context.start();
		
		try {
			// 为保证服务一直开着，利用输入流的阻塞来模拟  
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
